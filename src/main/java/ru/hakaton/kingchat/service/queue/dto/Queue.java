package ru.hakaton.kingchat.service.queue.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Queue {
    private Long id;
    private String queueNumber;
    private LocalDateTime regTime;
    private LocalDateTime queueTime;
}
