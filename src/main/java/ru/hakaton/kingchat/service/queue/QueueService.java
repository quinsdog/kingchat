package ru.hakaton.kingchat.service.queue;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.hakaton.kingchat.dao.model.Office;
import ru.hakaton.kingchat.dao.model.OfficeQueue;
import ru.hakaton.kingchat.dao.repository.OfficeRepository;
import ru.hakaton.kingchat.dao.repository.QueueRepository;
import ru.hakaton.kingchat.exception.NotAvailableTimeException;
import ru.hakaton.kingchat.service.queue.dto.Queue;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
@Slf4j
public class QueueService {

    private Map<String, AtomicInteger> queueNumberMap = new ConcurrentHashMap<>();
    private Map<String, AtomicInteger> queueSizeMap = new ConcurrentHashMap<>();

    @Value("${kingchat.service.time:10}")
    private int serviceTime;

    @Autowired
    private QueueRepository queueRepository;

    @Autowired
    private OfficeRepository officeRepository;

    @PostConstruct
    private void init() {
        generateTestData();
    }

    private void generateTestData() {
        queueRepository.deleteAllInBatch();
        officeRepository.findAll().forEach(this::generateTestQueue);
    }

    //TODO в дальнейшем необходимо считывать реальную очередь офиса
    private void generateTestQueue(Office office) {
        Random random = new Random();
        int queueSize = random.nextInt(10);
        List<OfficeQueue> officeQueueList = new ArrayList<>();
        for (int i = 0; i < queueSize; i++) {
            officeQueueList.add(createOfficeQueue(office, "" + i));
        }
        log.debug("saving queue size {} for office {}", queueSize, office.getId());
        queueRepository.saveAll(officeQueueList);
    }

    private OfficeQueue createOfficeQueue(Office office, String phone) {
        queueNumberMap.computeIfAbsent(office.getId(), v -> new AtomicInteger(1));
        queueSizeMap.computeIfAbsent(office.getId(), v -> new AtomicInteger(0));
        OfficeQueue officeQueue = new OfficeQueue();
        officeQueue.setOffice(office);
        officeQueue.setClientPhone(phone);
        officeQueue.setRegTime(LocalDateTime.now());
        officeQueue.setQueueTime(calculateQueueTime(office, queueSizeMap.get(office.getId()).getAndIncrement()));
        officeQueue.setQueueNumber(generateQueueNumber(office, queueNumberMap.get(office.getId()).getAndIncrement()));
        officeQueue.setActive(true);
        return officeQueue;
    }

    private OfficeQueue createOfficeQueue(Office office, String phone, LocalDateTime desiredTime) {
        OfficeQueue officeQueue = createOfficeQueue(office, phone);
        officeQueue.setQueueTime(desiredTime);
        return officeQueue;
    }

    /*private OfficeQueue createOfficeQueue(Office office, String phone, String email) {
        OfficeQueue officeQueue = createOfficeQueue(office, phone);
        officeQueue.setClientEmail(email);
        return officeQueue;
    }*/

    private String generateQueueNumber(Office office, int queueNumber) {
        return String.format("%s_%d", office.getId(), queueNumber);
    }

    private LocalDateTime calculateQueueTime(Office office, int queueSize) {
        LocalDateTime nextQueueTime = LocalDateTime.now().plusMinutes(queueSize * serviceTime + 1);
        int counter = 1;
        while (!isTimeAvailable(office, nextQueueTime)) {
            nextQueueTime = LocalDateTime.now().plusMinutes((queueSize + counter++) * serviceTime + 1);
        }
        return nextQueueTime;
    }

    @Transactional
    public Queue addClient(Office office, String phoneNumber) {
        OfficeQueue savedQueue = queueRepository.save(createOfficeQueue(office, phoneNumber));
        return new Queue(savedQueue.getId(), savedQueue.getQueueNumber(), savedQueue.getRegTime(), savedQueue.getQueueTime());
    }

    /*@Transactional
    public Queue addClient(Office office, String phoneNumber, String email) {
        OfficeQueue savedQueue = queueRepository.save(createOfficeQueue(office, phoneNumber, email));
        return new Queue(savedQueue.getId(), savedQueue.getQueueNumber(), savedQueue.getRegTime(), savedQueue.getQueueTime());
    }*/

    @Transactional
    public Queue addClient(Office office, String phoneNumber, LocalDateTime desiredTime) {
        if (isTimeAvailable(office, desiredTime)) {
            OfficeQueue savedQueue = queueRepository.save(createOfficeQueue(office, phoneNumber, desiredTime));
            return new Queue(savedQueue.getId(), savedQueue.getQueueNumber(), savedQueue.getRegTime(), savedQueue.getQueueTime());
        } else {
            throw new NotAvailableTimeException();
        }
    }

    private boolean isTimeAvailable(Office office, LocalDateTime desiredTime) {
        LocalDateTime startDateTime = desiredTime.minusMinutes(serviceTime);
        LocalDateTime endDateTime = desiredTime.plusMinutes(serviceTime);
        List<OfficeQueue> existsQueuesList = queueRepository.findByOffice(office).stream()
                .filter(queue -> queue.getQueueTime().isAfter(startDateTime) && queue.getQueueTime().isBefore(endDateTime))
                .collect(Collectors.toList());
        return existsQueuesList.isEmpty();
    }

    public int getOfficeQueueSize(Office office) {
        return queueSizeMap.get(office.getId()).get();
    }

    @Transactional
    public void removeClient(String queueNumber) {
        OfficeQueue queue = queueRepository.findByQueueNumber(queueNumber);
        if (queue == null) {
            return;
        }
        queue.setActive(false);
    }

    @Scheduled(fixedDelay = 10 * 60 * 1000)
    @Transactional
    public void refreshQueueSizeMap() {
        officeRepository.findAll().forEach(this::updateOfficeQueueSize);
    }

    private void updateOfficeQueueSize(Office office) {
        int queueSize = (int)office.getOfficeQueue().stream().filter(OfficeQueue::isActive).count();
        AtomicInteger atomicInteger = queueSizeMap.get(office.getId());
        if (atomicInteger == null) {
            atomicInteger = new AtomicInteger(0);
            queueSizeMap.put(office.getId(), atomicInteger);
        }
        atomicInteger.set(queueSize);
    }
}
