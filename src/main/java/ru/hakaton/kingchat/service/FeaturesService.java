package ru.hakaton.kingchat.service;

import java.util.List;
import java.util.Map;

public interface FeaturesService {
    Map<String, List<String>> getAllFeatures();
}
