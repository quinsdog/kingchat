package ru.hakaton.kingchat.service;


import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import ru.hakaton.kingchat.parsing.dto.Companies;
import ru.hakaton.kingchat.parsing.dto.Company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class XmlParserFeaturesService implements FeaturesService {

    @Autowired
    private XmlParserFeaturesService self;

    @Override
    public Map<String, List<String>> getAllFeatures() {

        try{
            List<Company> companies = getCompanies();

            Map<String, List<String>> multimap = new HashMap<>();

            companies.stream()
                    .map(Company::getEnumeratedFeatures)
                    .flatMap(List::stream)
                    .distinct()
                    .forEach(keyValueItem -> {
                        List<String> strings = multimap.computeIfAbsent(keyValueItem.getName(), s -> new ArrayList<>());
                        strings.add(keyValueItem.getValue());
                    });

            return multimap;


        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    public List<Company> getCompanies() throws Exception{
        XmlMapper xmlMapper = new XmlMapper();

        return xmlMapper.readValue(new ClassPathResource("usib.xml").getFile(), Companies.class).getCompany();
    }


}
