package ru.hakaton.kingchat.service.here;


import com.fasterxml.jackson.databind.JsonNode;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;


@Path("/6.2")
public interface HereGeocodeApiClient {

    @GET
    @Path("/geocode.json?app_id=bVvreM2s0MTcYXPch5qR&app_code=Z40wuViLVba7okZrMUG2Kg")
    JsonNode calculateRoute(@QueryParam("searchtext") String address);
}
