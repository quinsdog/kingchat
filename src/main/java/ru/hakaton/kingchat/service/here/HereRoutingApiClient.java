package ru.hakaton.kingchat.service.here;


import ru.hakaton.kingchat.service.here.dto.route.ResponseRoute;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;


@Path("/routing/7.2")
public interface HereRoutingApiClient {

    @GET
    @Path("/calculateroute.json?app_id=bVvreM2s0MTcYXPch5qR&app_code=Z40wuViLVba7okZrMUG2Kg")
    ResponseRoute calculateRoute(@QueryParam("waypoint0") String latLonString,
                                 @QueryParam("waypoint1") String latLongString,
                                 @QueryParam("mode") String modeString);

//    https://route.api.here.com/routing/7.2/calculateroute.json
//?app_id=6CcMS9LZrqbjSgbHsXEX
//&app_code=_WB8YHcZmq8SaspDXAZSow
//&waypoint0=geo!37.7914050,-122.3987030
//&waypoint1=geo!37.7866569,-122.4026513
//&jsonAttributes=41
//&mode=fastest;car;
}
