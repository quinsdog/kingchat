package ru.hakaton.kingchat.service.here.dto.route;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import ru.hakaton.kingchat.service.here.dto.Position;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class Maneuver {

    private Position position;
}
