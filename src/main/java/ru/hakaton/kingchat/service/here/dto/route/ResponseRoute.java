package ru.hakaton.kingchat.service.here.dto.route;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import ru.hakaton.kingchat.service.here.dto.Position;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class ResponseRoute {

    private Response response;

    public Long getDistance() {
        return response.getRoute().get(0).getSummary().getDistance();
    }

    public Long getTravelTime() {
        return response.getRoute().get(0).getSummary().getTravelTime();
    }

    public List<Position> getPositions() {
        return response.getRoute().get(0)
                .getLeg().stream()
                .map(Leg::getManeuver)
                .flatMap(Collection::stream)
                .map(Maneuver::getPosition)
                .collect(Collectors.toList());
    }
}
