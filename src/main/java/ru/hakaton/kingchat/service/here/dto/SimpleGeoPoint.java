package ru.hakaton.kingchat.service.here.dto;


import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class SimpleGeoPoint {
    private final Double latitude;
    private final Double longitude;

    @Override
    public String toString() {
        return String.format("%s,%s",latitude, longitude);
    }
}
