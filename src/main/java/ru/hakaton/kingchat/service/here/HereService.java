package ru.hakaton.kingchat.service.here;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.hakaton.kingchat.service.here.dto.Position;
import ru.hakaton.kingchat.service.here.dto.SimpleGeoPoint;
import ru.hakaton.kingchat.service.here.dto.route.ResponseRoute;

@Service
@Slf4j
public class HereService {
    private HereRoutingApiClient hereRoutingApiClient;
    private HereGeocodeApiClient hereGeocodeApiClient;


    public HereService(HereRoutingApiClient hereRoutingApiClient, HereGeocodeApiClient hereGeocodeApiClient) {
        this.hereRoutingApiClient = hereRoutingApiClient;
        this.hereGeocodeApiClient = hereGeocodeApiClient;
    }

    public SimpleGeoPoint geoCode(String searchText) {
        JsonNode jsonNode = hereGeocodeApiClient.calculateRoute(searchText);
        JsonNode firstFoundLocationNode = jsonNode.get("Response")
                .get("View")
                .get(0)
                .get("Result")
                .get(0)
                .get("Location")
                .get("NavigationPosition")
                .get(0);

        return new SimpleGeoPoint(firstFoundLocationNode.get("Latitude").asDouble(), firstFoundLocationNode.get("Longitude").asDouble());
    }

    public ResponseRoute calculateRoute(SimpleGeoPoint from, SimpleGeoPoint to) {

        final String stupidTemplate = "%s,%s";

        final String fromString = String.format(stupidTemplate, from.getLatitude(), from.getLongitude());
        final String toString = String.format(stupidTemplate, to.getLatitude(), to.getLongitude());

        log.info("From {}, To {}", fromString, toString);

        return hereRoutingApiClient.calculateRoute(fromString, toString, "fastest;car;traffic:disabled");
    }
}
