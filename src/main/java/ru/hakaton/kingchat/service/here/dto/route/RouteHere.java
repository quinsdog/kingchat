package ru.hakaton.kingchat.service.here.dto.route;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class RouteHere {

    private Summary summary;

    private List<Leg> leg;
}
