package ru.hakaton.kingchat.service.here.dto.route;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class Summary {

    private Long distance;
    private Long travelTime;
}
