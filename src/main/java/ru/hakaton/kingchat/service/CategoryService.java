package ru.hakaton.kingchat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.hakaton.kingchat.dao.model.Category;
import ru.hakaton.kingchat.dao.repository.CategoryRepository;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    public List<Category> getAllCategories() {
        Iterable<Category> all = categoryRepository.findAll();
        return StreamSupport.stream(all.spliterator(), false)
                .collect(Collectors.toList());
    }

    //заглуха
    public List<Category> getMostWandedCategories() {
        return getAllCategories().stream()
                .sorted(Comparator.comparing(Category::getId))
                .limit(5)
                .collect(Collectors.toList());
    }
}
