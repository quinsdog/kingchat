package ru.hakaton.kingchat.service.offices;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.hakaton.kingchat.dao.model.Category;
import ru.hakaton.kingchat.dao.model.Office;
import ru.hakaton.kingchat.dao.repository.OfficeRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class OfficeService {

    @Autowired
    private OfficeRepository officeRepository;

    public Office findOffice(String bankId) {
        return officeRepository.findOffice(bankId);
    }

    public List<Office> findOfficesInRadius(float minLat, float maxLat, float minLon, float maxLon) {
        log.trace("searching offices minLat = [{}], maxLat = [{}], minLon = [{}], maxLon = [{}]", minLat, maxLat, minLon, maxLon);
        List<Office> officeInRadius = officeRepository.findOfficesInRadius(minLat, maxLat, minLon, maxLon);
        log.trace("offices found = {}", officeInRadius.size());

        return officeInRadius;
    }

    @Transactional
    public List<Office> findInRadiusWithFilter(OfficeFilter filter) {
        List<Office> officesInRadius = findOfficesInRadius(filter.getMinLat(), filter.getMaxLat(), filter.getMinLon(), filter.getMaxLon());

        List<Office> collect = officesInRadius.stream()
                .filter(office -> {
                    List<Long> officeCategories = office.getCategories().stream()
                            .map(Category::getId)
                            .collect(Collectors.toList());

                    return officeCategories.containsAll(filter.getCategoryIds());
                })
                .collect(Collectors.toList());

        log.debug("found {} offices", collect.size());

        return collect;
    }
}
