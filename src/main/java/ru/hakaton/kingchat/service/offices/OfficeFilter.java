package ru.hakaton.kingchat.service.offices;

import lombok.*;

import java.util.Collections;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class OfficeFilter {
    private float minLat;
    private float maxLat;
    private float minLon;
    private float maxLon;
    private List<Long> categoryIds = Collections.emptyList();
}
