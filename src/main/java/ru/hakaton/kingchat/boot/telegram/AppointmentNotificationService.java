package ru.hakaton.kingchat.boot.telegram;

import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import ru.hakaton.kingchat.dao.model.BankAppointment;
import ru.hakaton.kingchat.dao.repository.BankAppointmentRepository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class AppointmentNotificationService {

    @Resource
    private BankAppointmentRepository bankAppointmentRepository;

    @Resource
    private CommandHandlerBot commandHandlerBot;

    @Scheduled(fixedDelay = 60000L)
    public void notifyAboutAppointment() {
        List<BankAppointment> expiredAppointments = new ArrayList<>();

        bankAppointmentRepository.findAppointmentsForNotification()
                .forEach(app -> {
                    try {
                        if (new DateTime().plusHours(1)
                                .isAfter(new DateTime(app.getAppointmentDate()))) {
                            commandHandlerBot.execute(new SendMessage()
                                    .setChatId(app.getChatId())
                                    .setText("Вы пропустили вашу очередь!"));
                            app.setExpired(true);
                            expiredAppointments.add(app);
                        }

                        int min = new Interval(new DateTime().plusHours(1), new DateTime(app.getAppointmentDate()))
                                .toPeriod()
                                .getMinutes();

                        if (min < 3 && min != 0) {
                            commandHandlerBot.execute(new SendMessage()
                                    .setChatId(app.getChatId())
                                    .setText("До вашей очереди осталось " + min + (min == 1 ? " минута" : "минуты")));
                        }

                    } catch (Exception ex) {
                        log.debug("scheduler problem", ex);
                    }
                });

        bankAppointmentRepository.saveAll(expiredAppointments);
    }
}
