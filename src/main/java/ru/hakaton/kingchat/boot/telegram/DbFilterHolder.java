package ru.hakaton.kingchat.boot.telegram;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import ru.hakaton.kingchat.dao.model.UserData;
import ru.hakaton.kingchat.dao.repository.UserDataRepository;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Slf4j
public class DbFilterHolder implements FilterHolder {

    @Autowired
    private UserDataRepository userDataRepository;


    @Override
    public void addFilter(Long chatId, String filter) {
        Set<String> filters = getFilters(chatId);

        if (filters.contains(filter)) {
            filters.remove(filter);
        } else {
            filters.add(filter);
        }

        UserData userData = userDataRepository.findByChatId(chatId);
        userData = userData != null ? userData : new UserData();
        userData.setChatId(chatId);

        String filterString = filters.stream().collect(Collectors.joining(","));

        if (!StringUtils.isEmpty(filterString)) {
            userData.setFilters(filterString);
        }

        userDataRepository.save(userData);
    }

    @Override
    public Set<String> getFilters(Long chatId) {
        return Optional.ofNullable(userDataRepository.findByChatId(chatId))
                .map(UserData::getFilters)
                .map(s -> Arrays.stream(s.split(","))
                        .collect(Collectors.toSet()))
                .orElse(new HashSet<>());
    }

    @Override
    @Transactional
    public void clearFilters(Long chatId) {
        UserData byChatId = userDataRepository.findByChatId(chatId);
        if(byChatId == null) return;

        byChatId.setFilters("");
        userDataRepository.save(byChatId);
    }
}
