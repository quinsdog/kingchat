package ru.hakaton.kingchat.boot.telegram;

import com.google.common.collect.ImmutableMap;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.ApiContext;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.hakaton.kingchat.boot.telegram.handler.CallbackHandler;
import ru.hakaton.kingchat.boot.telegram.handler.MessageHandler;

import java.util.List;

@Slf4j
public class CommandHandlerBot extends TelegramLongPollingBot {

    private final String botName;
    private final String botToken;

    private final ImmutableMap<String, MessageHandler> messageHandlers;
    private final ImmutableMap<String, CallbackHandler> callbackHandlers;

    private final MessageHandler locationHandler;
    private final MessageHandler defaultHandler;
    private final MessageHandler contactHandler;

    @Builder
    public CommandHandlerBot(DefaultBotOptions defaultBotOptions,
                             String botName,
                             String botToken,
                             ImmutableMap<String, MessageHandler> messageHandlers,
                             ImmutableMap<String, CallbackHandler> callbackHandlers,
                             MessageHandler locationHandler,
                             MessageHandler contactHandler,
                             MessageHandler defaultHandler) {
        super(defaultBotOptions != null ? defaultBotOptions : ApiContext.getInstance(DefaultBotOptions.class));
        Assert.notNull(botName, "notName is null");
        this.botName = botName;
        Assert.notNull(botToken, "botToken is null");
        this.botToken = botToken;
        Assert.notNull(messageHandlers, "messageHandlers is null");
        this.messageHandlers = messageHandlers;
        Assert.notNull(messageHandlers, "callbackHandlers is null");
        this.callbackHandlers = callbackHandlers;
        Assert.notNull(locationHandler, "locationHandler is null");
        this.locationHandler = locationHandler;
        Assert.notNull(contactHandler, "contactHandler is null");
        this.contactHandler = contactHandler;
        Assert.notNull(defaultHandler, "defaultHandler is null");
        this.defaultHandler = defaultHandler;
    }

    @Override
    public String getBotUsername() {
        return botName;
    }

    @Override
    public String getBotToken() {
        return botToken;
    }

    @Override
    public void onUpdateReceived(Update update) {
        try {
            log.debug("receive update [{}]", update);

            if (!update.hasMessage() && !update.hasCallbackQuery()) {
                log.debug("No message text or callback query in received update, so we simply skip that command.");
                // no honey - no money
                return;
            }

            if (update.hasMessage()) {

                List<BotApiMethod> responseBootApiMethodList;
                String messageText = update.getMessage().getText();
                if (!StringUtils.isEmpty(messageText)) {
                    MessageHandler currentCommandHandler = messageHandlers.get(messageText.trim());
                    if (currentCommandHandler != null) {
                        log.debug("message handler for user command [{}] found", messageText);
                        responseBootApiMethodList = currentCommandHandler.handle(update.getMessage());
                    } else {
                        log.debug("no message handler for user command [{}] found, using default message handler", messageText);
                        responseBootApiMethodList = defaultHandler.handle(update.getMessage());
                    }
                } else if (update.getMessage().getLocation() != null) {
                    log.debug("message contain location, so we use location handler");
                    responseBootApiMethodList = locationHandler.handle(update.getMessage());
                } else if (update.getMessage().getContact() != null) {
                    log.debug("message contain contact, so we use contact handler");
                    responseBootApiMethodList = contactHandler.handle(update.getMessage());
                } else {
                    log.debug("no suitable handlers for message, skip");
                    return;
                }

                responseBootApiMethodList.stream()
                        .forEach(responseBootApiMethod -> {
                            try {
                                execute(responseBootApiMethod);
                            } catch (Exception ex) {
                                throw new RuntimeException(ex);
                            }
                        });
                return;
            }

            String callbackType = update.getCallbackQuery().getData().split(":")[0];

            CallbackHandler currentCallbackHandler = callbackHandlers.get(callbackType.trim());
            if (currentCallbackHandler != null) {
                log.debug("message handler for user command [{}] found", callbackType);
                List callbackResponseMethodList = currentCallbackHandler.handle(update.getCallbackQuery());

                callbackResponseMethodList.stream()
                        .forEach(responseBootApiMethod -> {
                            try {
                                if (responseBootApiMethod instanceof SendPhoto) {
                                    execute((SendPhoto) responseBootApiMethod);
                                } else {
                                    execute((BotApiMethod) responseBootApiMethod);
                                }
                            } catch (Exception ex) {
                                throw new RuntimeException(ex);
                            }
                        });
                return;
            }

            log.debug("no callback handler for callback type [{}] found, skip", callbackType);

        } catch (Exception ex) {
            log.error("Something went wrong on [" + getClass().getSimpleName() + "] boot update recveive", ex);
        }
    }

    @Override
    public void onUpdatesReceived(List<Update> updates) {
        updates.stream().forEach(this::onUpdateReceived);
    }

}
