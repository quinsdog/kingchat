package ru.hakaton.kingchat.boot.telegram;

import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import ru.hakaton.kingchat.boot.telegram.handler.CallbackHandler;
import ru.hakaton.kingchat.boot.telegram.handler.ContactHandler;
import ru.hakaton.kingchat.boot.telegram.handler.LocationHandler;
import ru.hakaton.kingchat.boot.telegram.handler.MessageHandler;
import ru.hakaton.kingchat.boot.telegram.handler.callback.*;
import ru.hakaton.kingchat.boot.telegram.handler.message.*;
import ru.hakaton.kingchat.dao.repository.BankAppointmentRepository;
import ru.hakaton.kingchat.dao.repository.UserDataRepository;
import ru.hakaton.kingchat.dao.repository.UserDestinationHistoryRepository;
import ru.hakaton.kingchat.service.CategoryService;
import ru.hakaton.kingchat.service.offices.OfficeService;
import ru.hakaton.kingchat.service.queue.QueueService;

import java.util.Collections;

@Configuration
public class KingchatBootConfiguration {

    @Value("${telegrambot.socksProxy.host:}")
    private String proxyHost;
    @Value("${telegrambot.socksProxy.port:9050}")
    private Integer proxyPort;

    @Value("${telegrambot.socksProxy.enabled:false}")
    private Boolean proxyEnabled;

    @Autowired
    private DbFilterHolder dbFilterHolder;

    @Bean
    @Autowired
    public CommandHandlerBot kingchatBot(Environment environment, OfficeService officeService,
                                         CategoryService categoryService,
                                         UserDataRepository userDataRepository,
                                         BankAppointmentRepository bankAppointmentRepository,
                                         UserDestinationHistoryRepository userDestinationHistoryRepository,
                                         QueueService queueService) {

        DefaultBotOptions kingchatBotOptions = new DefaultBotOptions();

        if(proxyEnabled) {
            kingchatBotOptions.setProxyType(DefaultBotOptions.ProxyType.SOCKS5);
            kingchatBotOptions.setProxyHost(proxyHost);
            kingchatBotOptions.setProxyPort(proxyPort);
        }

        String botName = environment.getRequiredProperty("kingchat.boot-name");
        String botToken = environment.getRequiredProperty("kingchat.boot-token");

        FilterHolder organizationFilterHolder = dbFilterHolder;
        FilterHolder personFilterHolder = dbFilterHolder;

        return CommandHandlerBot.builder()
                .defaultBotOptions(kingchatBotOptions)
                .botName(botName)
                .botToken(botToken)
                .messageHandlers(ImmutableMap.<String, MessageHandler>builder()
                        .put(Constant.START_COMMAND, new StartCommandHandler(
                                "Мы рады приветствовать вас на канале KING-CHAT!\n" +
                                "Чтобы начать, воспользуйтесь меню."))
                        .put(Constant.START_SEARCH_BRANCH_COMMAND, new BranchSearchHandler())
                        .put(Constant.START_SEARCH_PERSON_BRANCH_COMMAND, new PersonBranchHandler(personFilterHolder, categoryService))
                        .put(Constant.START_SEARCH_ORGANIZATION_BRANCH_COMMAND, new OrganizationBranchHandler(organizationFilterHolder, categoryService))
                        .put(Constant.BACK_TO_START_COMMAND, new StartCommandHandler("Главное меню."))
                        .put(Constant.CANCEL_APPOINTMENT_COMMAND, new CancelHandler(bankAppointmentRepository, queueService))
                        .build())
                .callbackHandlers(ImmutableMap.<String, CallbackHandler>builder()
                        .put(Constant.PERSON_BRANCH_FILTER_CALLBACK, new PersonFilterHandler(personFilterHolder, categoryService))
                        .put(Constant.ORGANIZATION_BRANCH_FILTER_CALLBACK, new OrganizationFilterHandler(organizationFilterHolder, categoryService))
                        .put(Constant.SIGN_ON_QUEUE_CALLBACK, new AppointmentHandler(bankAppointmentRepository))
                        .put(Constant.CHOSE_TIME_CALLBACK, new ChoseTimeCallback(bankAppointmentRepository, officeService, userDataRepository, queueService))
                        .put(Constant.CHOSE_BRANCH_CALLBACK, new ChoseBranchCallback(officeService))
                        .build())
                .locationHandler(new LocationHandler(officeService, userDestinationHistoryRepository, dbFilterHolder))
                .contactHandler(new ContactHandler(bankAppointmentRepository, userDataRepository))
                .defaultHandler((message) -> Collections.singletonList(new SendMessage()
                        .setChatId(message.getChatId())
                        .setText("Стандартное сообщение")))
                .build();
    }
}
