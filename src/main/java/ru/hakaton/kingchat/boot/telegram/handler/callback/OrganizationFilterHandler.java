package ru.hakaton.kingchat.boot.telegram.handler.callback;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import ru.hakaton.kingchat.boot.telegram.Constant;
import ru.hakaton.kingchat.boot.telegram.FilterHolder;
import ru.hakaton.kingchat.boot.telegram.Keyboards;
import ru.hakaton.kingchat.boot.telegram.handler.CallbackHandler;
import ru.hakaton.kingchat.boot.telegram.handler.CategoryFiltrable;
import ru.hakaton.kingchat.service.CategoryService;

import java.util.Collections;
import java.util.List;
import java.util.Set;

public class OrganizationFilterHandler implements CallbackHandler, CategoryFiltrable {

    private final FilterHolder filterHolder;
    private final CategoryService categoryService;

    public OrganizationFilterHandler(FilterHolder filterHolder, CategoryService categoryService) {
        this.filterHolder = filterHolder;
        this.categoryService = categoryService;
    }

    @Override
    public List<BotApiMethod> handle(CallbackQuery callbackQuery) throws Exception {

        filterHolder.addFilter(callbackQuery.getMessage().getChatId(), callbackQuery.getData());

        Keyboards.InlineBuilder inlineBuilder = Keyboards.inlineBuilder();
        Set<String> filters = filterHolder.getFilters(callbackQuery.getMessage().getChatId());

        addCategoriesToFilter(categoryService, Constant.ORGANIZATION_BRANCH_FILTER_CALLBACK, inlineBuilder, filters);

        return Collections.singletonList(new EditMessageText()
                .setChatId(callbackQuery.getMessage().getChatId().toString())
                .setInlineMessageId(callbackQuery.getInlineMessageId())
                .setText("Или воспользуйтесь фильтром:")
                .enableMarkdown(true)
                .setMessageId(callbackQuery.getMessage().getMessageId())
                .setReplyMarkup(inlineBuilder.build()));
    }


}
