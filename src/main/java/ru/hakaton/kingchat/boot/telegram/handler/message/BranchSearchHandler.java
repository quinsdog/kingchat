package ru.hakaton.kingchat.boot.telegram.handler.message;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import ru.hakaton.kingchat.boot.telegram.Constant;
import ru.hakaton.kingchat.boot.telegram.Keyboards;
import ru.hakaton.kingchat.boot.telegram.handler.MessageHandler;

import java.util.Collections;
import java.util.List;

public class BranchSearchHandler implements MessageHandler {

    @Override
    public List<BotApiMethod> handle(Message message) throws Exception {
        return Collections.singletonList(
                new SendMessage()
                        .setChatId(message.getChatId())
                        .setText("Пока произвольный текст")
                        .enableMarkdown(true)
                        .setReplyMarkup(Keyboards.replyBuilder()
                                .addButton(Constant.START_SEARCH_PERSON_BRANCH_COMMAND)
                                .addButton(Constant.START_SEARCH_ORGANIZATION_BRANCH_COMMAND)
                                .addButton(Constant.BACK_TO_START_COMMAND)
                                .build()));
    }
}
