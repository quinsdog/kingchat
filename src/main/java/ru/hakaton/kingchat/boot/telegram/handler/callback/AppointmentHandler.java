package ru.hakaton.kingchat.boot.telegram.handler.callback;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import ru.hakaton.kingchat.boot.telegram.Constant;
import ru.hakaton.kingchat.boot.telegram.Keyboards;
import ru.hakaton.kingchat.boot.telegram.handler.CallbackHandler;
import ru.hakaton.kingchat.dao.model.BankAppointment;
import ru.hakaton.kingchat.dao.repository.BankAppointmentRepository;

import java.util.Collections;
import java.util.List;

public class AppointmentHandler implements CallbackHandler {

    private final BankAppointmentRepository bankAppointmentRepository;

    public AppointmentHandler(BankAppointmentRepository bankAppointmentRepository) {
        this.bankAppointmentRepository = bankAppointmentRepository;
    }

    @Override
    public List<BotApiMethod> handle(CallbackQuery callbackQuery) throws Exception {
        BankAppointment bankAppointment = bankAppointmentRepository
                .findCurrentBankAppointment(callbackQuery.getMessage().getChatId());
        if (bankAppointment != null && bankAppointment.getAppointmentDate() != null) {
            return Collections.singletonList(new SendMessage()
                    .setChatId(callbackQuery.getMessage().getChatId())
                    .setText("Вы уже встали в очередь в банке " +
                            "Повторная регистрация может пройти только после отмены предыдущей. Отменить встречу?")
                    .enableMarkdown(true)
                    .setReplyMarkup(Keyboards.replyBuilder()
                            .addButton(Constant.CANCEL_APPOINTMENT_COMMAND)
                            .addButton(Constant.BACK_TO_START_COMMAND)
                            .build()));
        }

        bankAppointment = bankAppointment != null ? bankAppointment : new BankAppointment();

        bankAppointment.setChatId(callbackQuery.getMessage().getChatId());
        bankAppointment.setBankId(callbackQuery.getData().split(":")[1]);
        bankAppointment.setExpired(false);

        bankAppointmentRepository.save(bankAppointment);

        return Collections.singletonList(new SendMessage()
                .setChatId(callbackQuery.getMessage().getChatId())
                .setText("Теперь вы можете выбрать удобное время, для посещения")
                .enableMarkdown(true)
                .setReplyMarkup(Keyboards.replyBuilder()
                    .addRequestContactButton(Constant.CHOSE_TIME_COMMAND)
                    .addButton(Constant.BACK_TO_START_COMMAND)
                    .build()));
    }
}
