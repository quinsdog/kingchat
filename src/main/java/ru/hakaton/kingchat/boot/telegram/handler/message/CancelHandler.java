package ru.hakaton.kingchat.boot.telegram.handler.message;

import lombok.extern.slf4j.Slf4j;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import ru.hakaton.kingchat.boot.telegram.Constant;
import ru.hakaton.kingchat.boot.telegram.Keyboards;
import ru.hakaton.kingchat.boot.telegram.handler.MessageHandler;
import ru.hakaton.kingchat.dao.model.BankAppointment;
import ru.hakaton.kingchat.dao.repository.BankAppointmentRepository;
import ru.hakaton.kingchat.service.queue.QueueService;

import java.util.Collections;
import java.util.List;

@Slf4j
public class CancelHandler implements MessageHandler {

    private final BankAppointmentRepository bankAppointmentRepository;
    private final QueueService queueService;

    public CancelHandler(BankAppointmentRepository bankAppointmentRepository, QueueService queueService) {
        this.bankAppointmentRepository = bankAppointmentRepository;
        this.queueService = queueService;
    }

    @Override
    public List<BotApiMethod> handle(Message message) throws Exception {
        BankAppointment bankAppointment = bankAppointmentRepository
                .findCurrentBankAppointment(message.getChatId());

        if (bankAppointment == null) {
            return Collections.singletonList(new SendMessage()
                    .setChatId(message.getChatId())
                    .setText("Что-то пошло не так. Повторите процедуру заново, извиняемся за неудобаства.")
                    .enableMarkdown(true));
        }

        bankAppointment.setExpired(true);

        try {
            queueService.removeClient(bankAppointment.getBankPseudoCode());
        } catch (Exception ex) {
            log.error("queue deletion exception ", ex);
        }
        bankAppointmentRepository.save(bankAppointment);

        BankAppointment newBankAppointment = new BankAppointment();
        newBankAppointment.setChatId(bankAppointment.getChatId());
        newBankAppointment.setBankId(bankAppointment.getBankId());
        newBankAppointment.setExpired(false);

        bankAppointmentRepository.save(newBankAppointment);

        return Collections.singletonList(new SendMessage()
                .setChatId(message.getChatId())
                .setText("Вас убрали из очереди банка.")
                .enableMarkdown(true)
                .setReplyMarkup(Keyboards.replyBuilder()
                        .addRequestContactButton(Constant.CHOSE_TIME_COMMAND)
                        .addButton(Constant.BACK_TO_START_COMMAND)
                        .build()));
    }
}
