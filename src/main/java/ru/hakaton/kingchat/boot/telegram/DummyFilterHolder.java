package ru.hakaton.kingchat.boot.telegram;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class DummyFilterHolder implements FilterHolder {
    private final ConcurrentHashMap<Long, HashSet<String>> personalFilters =
            new ConcurrentHashMap<>();

    @Override
    public void addFilter(Long chatId, String filter) {
        personalFilters.compute(chatId, (key, value) -> {
            if (value == null) {
                HashSet<String> chatFilters = new HashSet<>();
                chatFilters.add(filter);
                return chatFilters;
            }

            if (value.contains(filter)) {
                value.remove(filter);
            } else {
                value.add(filter);
            }

            return value;
        });
    }

    @Override
    public Set<String> getFilters(Long chatId) {
        HashSet<String> filters = personalFilters.get(chatId);
        return filters != null ? filters : new HashSet<>();
    }

    @Override
    public void clearFilters(Long chatId) {
        HashSet<String> strings = personalFilters.get(chatId);
        if (strings == null) return;

        strings.clear();
    }
}
