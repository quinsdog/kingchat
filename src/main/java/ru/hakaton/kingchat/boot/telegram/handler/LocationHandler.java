package ru.hakaton.kingchat.boot.telegram.handler;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendVenue;
import org.telegram.telegrambots.meta.api.objects.Message;
import ru.hakaton.kingchat.boot.telegram.Constant;
import ru.hakaton.kingchat.boot.telegram.FilterHolder;
import ru.hakaton.kingchat.boot.telegram.Keyboards;
import ru.hakaton.kingchat.dao.model.Office;
import ru.hakaton.kingchat.dao.model.UserDestinationHistory;
import ru.hakaton.kingchat.dao.repository.UserDestinationHistoryRepository;
import ru.hakaton.kingchat.service.offices.OfficeFilter;
import ru.hakaton.kingchat.service.offices.OfficeService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class LocationHandler implements MessageHandler {

    private final float RADIUS_IN_KM = 50;

    private final OfficeService officeService;
    private final UserDestinationHistoryRepository userDestinationHistoryRepository;
    private FilterHolder filterHolder; // пока только для физиков, нет времени

    public LocationHandler(OfficeService officeService, UserDestinationHistoryRepository userDestinationHistoryRepository, FilterHolder filterHolder) {
        this.officeService = officeService;
        this.userDestinationHistoryRepository = userDestinationHistoryRepository;
        this.filterHolder = filterHolder;
    }

    @Override
    public List<BotApiMethod> handle(Message message) throws Exception {

        Long chatId = message.getChatId();

        UserDestinationHistory userDestinationHistory = new UserDestinationHistory();
        userDestinationHistory.setChatId(chatId);
        userDestinationHistory.setLatitude(message.getLocation().getLatitude().doubleValue());
        userDestinationHistory.setLongitude(message.getLocation().getLongitude().doubleValue());

        userDestinationHistoryRepository.save(userDestinationHistory);

        float kmInLongitudeDegree = (float) (111.320D * Math.cos( message.getLocation().getLatitude() / 180.0D * Math.PI));

        float deltaLat = RADIUS_IN_KM / 111.1F;
        float deltaLon = RADIUS_IN_KM / kmInLongitudeDegree;

        float minLat = message.getLocation().getLatitude() - deltaLat;
        float maxLat = message.getLocation().getLatitude() + deltaLat;
        float minLon = message.getLocation().getLongitude() - deltaLon;
        float maxLon = message.getLocation().getLongitude() + deltaLon;

        ArrayList<BotApiMethod> locationSelection = new ArrayList<>();

        Comparator<Office> distanceComparator = (a, b) -> {
            double firstDistance = Math.sqrt(Math.pow(a.getLatitude() - message.getLocation().getLatitude(), 2) +
                    Math.pow(a.getLongitude() - message.getLocation().getLongitude(), 2));
            double secondDistance = Math.sqrt(Math.pow(b.getLatitude() - message.getLocation().getLatitude(), 2) +
                    Math.pow(b.getLongitude() - message.getLocation().getLongitude(), 2));

            if (firstDistance > secondDistance) {
                return 1;
            } else if (firstDistance == secondDistance) {
                return 0;
            } else {
                return -1;
            }
        };


        Keyboards.InlineBuilder inlineBuilder = Keyboards.inlineBuilder();

        for (Office office : getOffices(minLat, maxLat, minLon, maxLon, chatId)
                .stream()
                .sorted(distanceComparator)
                .limit(3)
                .collect(Collectors.toList())) {
            if (office.getLatitude() == null || office.getLongitude() == null) {
                continue;
            }

            inlineBuilder.addButton(office.getName(), Constant.CHOSE_BRANCH_CALLBACK + ":" + office.getId());



//
//            locationSelection.add(new SendVenue()
//                .setTitle(office.getName())
//                .setAddress(office.getAddres())
//                .setChatId(message.getChatId())
//                .setLatitude(office.getLatitude().floatValue())
//                .setLongitude(office.getLongitude().floatValue()));
//
//            locationSelection.add(new SendMessage()
//                .setChatId(message.getChatId())
//                .setText("id = " + office.getId() + "\n" +
//                        "name = " + office.getName() + "\n" +
//                        "address = " + office.getAddres() + "\n" +
//                        "country = " + office.getCountry() + "\n" +
//                        "linkUrl = " + office.getLinkUrl() + "\n" +
//                        "workingTimeRaw = " + office.getWorkingTimeRaw())
//                .enableMarkdown(true)
//                .setReplyMarkup(Keyboards.inlineBuilder()
//                        .addButton("Встать в очередь", Constant.SIGN_ON_QUEUE_CALLBACK + ":" + office.getId())
//                        .build()));

        }



        return Collections.singletonList(new SendMessage()
            .setChatId(message.getChatId())
            .setText("Найденные отделения:")
            .enableMarkdown(true)
            .setReplyMarkup(inlineBuilder.build()));
    }

    private List<Office> getOffices(float minLat, float maxLat, float minLon, float maxLon, Long chatId) {
        OfficeFilter officeFilter = new OfficeFilter();
        officeFilter.setMinLat(minLat);
        officeFilter.setMaxLat(maxLat);
        officeFilter.setMinLon(minLon);
        officeFilter.setMaxLon(maxLon);

        List<Long> collect = filterHolder.getFilters(chatId).stream()
                .map(filter -> filter.split(":")[1])
                .map(Long::valueOf)
                .distinct()
                .collect(Collectors.toList());

        officeFilter.setCategoryIds(collect);

        return officeService.findInRadiusWithFilter(officeFilter);
    }
}
