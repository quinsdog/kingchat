package ru.hakaton.kingchat.boot.telegram.handler.callback;

import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import ru.hakaton.kingchat.QRHelper;
import ru.hakaton.kingchat.boot.telegram.Constant;
import ru.hakaton.kingchat.boot.telegram.Keyboards;
import ru.hakaton.kingchat.boot.telegram.handler.CallbackHandler;
import ru.hakaton.kingchat.dao.model.BankAppointment;
import ru.hakaton.kingchat.dao.repository.BankAppointmentRepository;
import ru.hakaton.kingchat.dao.repository.UserDataRepository;
import ru.hakaton.kingchat.exception.NotAvailableTimeException;
import ru.hakaton.kingchat.service.offices.OfficeService;
import ru.hakaton.kingchat.service.queue.QueueService;
import ru.hakaton.kingchat.service.queue.dto.Queue;

import java.io.ByteArrayInputStream;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Slf4j
public class ChoseTimeCallback implements CallbackHandler {

    private final BankAppointmentRepository bankAppointmentRepository;
    private final OfficeService officeService;
    private final UserDataRepository userDataRepository;
    private final QueueService queueService;

    public ChoseTimeCallback(BankAppointmentRepository bankAppointmentRepository,
                             OfficeService officeService,
                             UserDataRepository userDataRepository,
                             QueueService queueService) {
        this.bankAppointmentRepository = bankAppointmentRepository;
        this.officeService = officeService;
        this.userDataRepository = userDataRepository;
        this.queueService = queueService;
    }

    @Override
    public List<BotApiMethod> handle(CallbackQuery callbackQuery) throws Exception {
        BankAppointment bankAppointment = bankAppointmentRepository
                .findCurrentBankAppointment(callbackQuery.getMessage().getChatId());

        if (bankAppointment == null) {
            return Collections.singletonList(new SendMessage()
                    .setChatId(callbackQuery.getMessage().getChatId())
                    .setText("Что-то пошло не так. Повторите процедуру заново, извиняемся за неудобаства.")
                    .enableMarkdown(true));
        }

        if (bankAppointment.getAppointmentDate() != null) {
            return Collections.singletonList(new SendMessage()
                    .setChatId(callbackQuery.getMessage().getChatId())
                    .setText("Вы уже встали в очередь в банке " +
                            "Повторная регистрация может пройти только после отмены предыдущей. Отменить встречу?")
                    .enableMarkdown(true)
                    .setReplyMarkup(Keyboards.replyBuilder()
                            .addButton(Constant.CANCEL_APPOINTMENT_COMMAND)
                            .addButton(Constant.BACK_TO_START_COMMAND)
                            .build()));
        }

        String[] callbackArray = callbackQuery.getData().split(":");

        DateTime appointmentDate = DateTime.now()
                .withHourOfDay(Integer.parseInt(callbackArray[1]))
                .withMinuteOfHour(Integer.parseInt(callbackArray[2]));

        if (DateTime.now().plusHours(1).isAfter(appointmentDate)) {
            return Collections.singletonList(new SendMessage()
                    .setChatId(callbackQuery.getMessage().getChatId())
                    .setText("Указанное время уже недоступно. Выберете снова, пожалуйста.")
                    .enableMarkdown(true));
        }

        Queue queue = null;
        try {
            queue = queueService.addClient(officeService.findOffice(bankAppointment.getBankId()),
                    userDataRepository.findByChatId(callbackQuery.getMessage().getChatId()).getPhone(),
                    LocalDateTime.ofInstant(appointmentDate.toDate().toInstant(), ZoneId.systemDefault()));
        } catch (NotAvailableTimeException ex) {
            log.error("", ex);
        }

        bankAppointment.setAppointmentDate(appointmentDate.toDate());
        bankAppointment.setBankPseudoCode(queue != null ? queue.getQueueNumber() : Math.abs(new Random().nextInt() % 100000) + "");

        bankAppointmentRepository.save(bankAppointment);

        try {
            List botApiMethods = new ArrayList<>();

            botApiMethods.add(new SendMessage()
                    .setChatId(callbackQuery.getMessage().getChatId())
                    .setText("Готово. Ваш талончик: " + bankAppointment.getBankPseudoCode().replaceAll("_", ""))
                    .enableMarkdown(true)
                    .setReplyMarkup(Keyboards.replyBuilder()
                            .addButton(Constant.CANCEL_APPOINTMENT_COMMAND)
                            .addButton(Constant.BACK_TO_START_COMMAND)
                            .build()));

            botApiMethods.add(new SendPhoto()
                    .setChatId(callbackQuery.getMessage().getChatId())
                    .setPhoto(new InputFile(new ByteArrayInputStream(
                            QRHelper.generateQRCodeImagePng(bankAppointment.getBankPseudoCode().replaceAll("_", ""), 400, 400)), "талончик.png")));

            return botApiMethods;
        } catch (Exception ex) {
            return null;
        }
    }
}
