package ru.hakaton.kingchat.boot.telegram;

public interface Constant {
    String START_COMMAND = "/start";

    String START_SEARCH_BRANCH_COMMAND = "Отделения";
    String START_SEARCH_ATM_COMMAND = "Банкоматы";

    String START_SEARCH_PERSON_BRANCH_COMMAND = "Физическое лицо";
    String START_SEARCH_ORGANIZATION_BRANCH_COMMAND = "Юридических лицо";

    String GIVE_LOCATION_COMMAND = "Передать текущее местоположение";

    String CHOSE_TIME_COMMAND = "Выбрать время";

    String CANCEL_APPOINTMENT_COMMAND = "Отменить";

    String BACK_TO_START_COMMAND = "На главную";

    String PERSON_BRANCH_FILTER_CALLBACK = "PERSON_BRANCH_FILTER_CALLBACK";
    String ORGANIZATION_BRANCH_FILTER_CALLBACK = "ORGANIZATION_BRANCH_FILTER_CALLBACK";

    String SIGN_ON_QUEUE_CALLBACK = "SIGN_ON_QUEUE_CALLBACK";
    String CHOSE_TIME_CALLBACK = "CHOSE_TIME_CALLBACK";

    String CHOSE_TIME_BY_INTER_CALLBACK = "CHOSE_TIME_BY_INTER_CALLBACK";

    String CHOSE_BRANCH_CALLBACK = "CHOSE_BRANCH_CALLBACK";
}
