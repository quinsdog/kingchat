package ru.hakaton.kingchat.boot.telegram.handler.callback;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendVenue;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import ru.hakaton.kingchat.boot.telegram.Constant;
import ru.hakaton.kingchat.boot.telegram.FilterHolder;
import ru.hakaton.kingchat.boot.telegram.Keyboards;
import ru.hakaton.kingchat.boot.telegram.handler.CallbackHandler;
import ru.hakaton.kingchat.dao.model.Office;
import ru.hakaton.kingchat.dao.repository.UserDestinationHistoryRepository;
import ru.hakaton.kingchat.service.offices.OfficeService;

import java.util.Arrays;
import java.util.List;

public class ChoseBranchCallback implements CallbackHandler {

    private final OfficeService officeService;

    public ChoseBranchCallback(OfficeService officeService) {
        this.officeService = officeService;
    }

    @Override
    public List<BotApiMethod> handle(CallbackQuery callbackQuery) throws Exception {

        String bankId = callbackQuery.getData().split(":")[1];

        Office office = officeService.findOffice(bankId);

        return Arrays.asList(
                new SendVenue()
                .setTitle(office.getName())
                .setAddress(office.getAddres())
                .setChatId(callbackQuery.getMessage().getChatId())
                .setLatitude(office.getLatitude().floatValue())
                .setLongitude(office.getLongitude().floatValue()),
                new SendMessage()
                .setChatId(callbackQuery.getMessage().getChatId())
                .setText("id = " + office.getId() + "\n" +
                        "name = " + office.getName() + "\n" +
                        "address = " + office.getAddres() + "\n" +
                        "country = " + office.getCountry() + "\n" +
                        "linkUrl = " + office.getLinkUrl() + "\n" +
                        "workingTimeRaw = " + office.getWorkingTimeRaw())
                .enableMarkdown(true)
                .setReplyMarkup(Keyboards.inlineBuilder()
                        .addButton("Встать в очередь", Constant.SIGN_ON_QUEUE_CALLBACK + ":" + office.getId())
                        .build())
        );
    }
}
