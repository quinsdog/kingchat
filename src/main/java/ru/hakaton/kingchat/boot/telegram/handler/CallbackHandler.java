package ru.hakaton.kingchat.boot.telegram.handler;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.List;

public interface CallbackHandler {
    List<BotApiMethod> handle(CallbackQuery callbackQuery) throws Exception;
}
