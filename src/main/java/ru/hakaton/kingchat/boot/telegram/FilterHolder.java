package ru.hakaton.kingchat.boot.telegram;

import java.util.Set;

public interface FilterHolder {
    void addFilter(Long chatId, String filter);

    Set<String> getFilters(Long chatId);

    void clearFilters(Long chatId);
}
