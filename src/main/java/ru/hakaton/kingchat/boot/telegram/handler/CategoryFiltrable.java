package ru.hakaton.kingchat.boot.telegram.handler;

import ru.hakaton.kingchat.boot.telegram.Keyboards;
import ru.hakaton.kingchat.service.CategoryService;

import java.util.Set;

public interface CategoryFiltrable {
    default void addCategoriesToFilter(CategoryService categoryService, String callbackDataPrefix, Keyboards.InlineBuilder inlineBuilder, Set<String> filters) {

        categoryService.getMostWandedCategories()
                .forEach(category ->
                        inlineBuilder.addButtonMarkedByFilter(category.getValue(), callbackDataPrefix + ":" + category.getId(), filters)
                );
    };
}
