package ru.hakaton.kingchat.boot.telegram.handler;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import ru.hakaton.kingchat.boot.telegram.Constant;
import ru.hakaton.kingchat.boot.telegram.Keyboards;
import ru.hakaton.kingchat.dao.model.BankAppointment;
import ru.hakaton.kingchat.dao.model.UserData;
import ru.hakaton.kingchat.dao.repository.BankAppointmentRepository;
import ru.hakaton.kingchat.dao.repository.UserDataRepository;

import java.util.Collections;
import java.util.List;

public class ContactHandler implements MessageHandler {

    private final static DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormat.forPattern("HH:mm");

    private final BankAppointmentRepository bankAppointmentRepository;
    private final UserDataRepository userDataRepository;

    public ContactHandler(BankAppointmentRepository bankAppointmentRepository, UserDataRepository userDataRepository) {
        this.bankAppointmentRepository = bankAppointmentRepository;
        this.userDataRepository = userDataRepository;
    }

    @Override
    public List<BotApiMethod> handle(Message message) throws Exception {
        BankAppointment bankAppointment = bankAppointmentRepository
                .findCurrentBankAppointment(message.getChatId());
        if (bankAppointment != null && bankAppointment.getAppointmentDate() != null) {
            return Collections.singletonList(new SendMessage()
                    .setChatId(message.getChatId())
                    .setText("Вы уже встали в очередь в банке " +
                            "Повторная регистрация может пройти только после отмены предыдущей. Отменить встречу?")
                    .enableMarkdown(true)
                    .setReplyMarkup(Keyboards.replyBuilder()
                            .addButton(Constant.CANCEL_APPOINTMENT_COMMAND)
                            .addButton(Constant.BACK_TO_START_COMMAND)
                            .build()));
        }


        UserData userData = userDataRepository.findByChatId(message.getChatId());
        userData = userData != null ? userData : new UserData();

        userData.setChatId(message.getChatId());
        userData.setPhone(message.getContact().getPhoneNumber());

        userDataRepository.save(userData);

        DateTime startTime = DateTime.now();
        startTime = startTime.plusHours(1).plusMinutes(25 - (startTime.getMinuteOfHour() % 10));

        Keyboards.InlineBuilder inlineBuilder = Keyboards.inlineBuilder();

        for (int i = 0; i<5; i++) {
            String tempTime = DATE_TIME_FORMATTER.print(startTime);
            inlineBuilder.addButton(tempTime,
                    Constant.CHOSE_TIME_CALLBACK + ":" + tempTime);
            startTime = startTime.plusMinutes(10);
        }

        return Collections.singletonList(new SendMessage()
                .setChatId(message.getChatId())
                .setText("Выберете удобное время")
                .enableMarkdown(true)
                .setReplyMarkup(inlineBuilder.build()));
    }
}
