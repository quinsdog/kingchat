package ru.hakaton.kingchat.boot.telegram;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Keyboards {

    public static ReplyBuilder replyBuilder() {
        return new ReplyBuilder();
    }

    public static InlineBuilder inlineBuilder() {
        return new InlineBuilder();
    }

    public static class ReplyBuilder {
        List<KeyboardRow> keyboardRowList = new ArrayList<>();

        public ReplyBuilder addButton(String text) {
            KeyboardRow row = new KeyboardRow();
            row.add(new KeyboardButton().setText(text));
            keyboardRowList.add(row);
            return this;
        }

        public ReplyBuilder addRequestLocationButton(String text) {
            KeyboardRow row = new KeyboardRow();
            row.add(new KeyboardButton().setText(text)
                    .setRequestLocation(true));
            keyboardRowList.add(row);
            return this;
        }

        public ReplyBuilder addRequestContactButton(String text) {
            KeyboardRow row = new KeyboardRow();
            row.add(new KeyboardButton().setText(text)
                    .setRequestContact(true));
            keyboardRowList.add(row);
            return this;
        }

        public ReplyKeyboardMarkup build() {
            ReplyKeyboardMarkup replyStartKeyboard = new ReplyKeyboardMarkup();
            replyStartKeyboard.setKeyboard(keyboardRowList);
            replyStartKeyboard.setResizeKeyboard(true);
            return replyStartKeyboard;
        }
    }

    public static class InlineBuilder {
        List<List<InlineKeyboardButton>> keyboardRowList = new ArrayList<>();

        public InlineBuilder addButtonMarkedByFilter(String text, String callbackData, Set<String> filter) {
            List<InlineKeyboardButton> row = new ArrayList<>();
            row.add(new InlineKeyboardButton().setText(filter.contains(callbackData) ? "+ " + text : text)
                    .setCallbackData(callbackData));
            keyboardRowList.add(row);

            return this;
        }

        public InlineBuilder addButton(String text, String callbackData) {

            List<InlineKeyboardButton> row = new ArrayList<>();
            row.add(new InlineKeyboardButton().setText(text)
                    .setCallbackData(callbackData));
            keyboardRowList.add(row);

            return this;
        }

        public InlineBuilder addRequestPhoneButton(String text, String callbackData) {

            List<InlineKeyboardButton> row = new ArrayList<>();
            row.add(new InlineKeyboardButton().setText(text)
                    .setCallbackData(callbackData));
            keyboardRowList.add(row);

            return this;
        }

        public InlineKeyboardMarkup build() {
            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
            inlineKeyboardMarkup.setKeyboard(keyboardRowList);
            return inlineKeyboardMarkup;
        }

    }
}
