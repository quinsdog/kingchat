package ru.hakaton.kingchat.boot.telegram.handler.message;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import ru.hakaton.kingchat.boot.telegram.Constant;
import ru.hakaton.kingchat.boot.telegram.FilterHolder;
import ru.hakaton.kingchat.boot.telegram.Keyboards;
import ru.hakaton.kingchat.boot.telegram.handler.CategoryFiltrable;
import ru.hakaton.kingchat.boot.telegram.handler.MessageHandler;
import ru.hakaton.kingchat.service.CategoryService;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class OrganizationBranchHandler implements MessageHandler, CategoryFiltrable {

    private final FilterHolder filterHolder;
    private final CategoryService categoryService;

    public OrganizationBranchHandler(FilterHolder filterHolder, CategoryService categoryService) {
        this.filterHolder = filterHolder;
        this.categoryService =  categoryService;
    }

    @Override
    public List<BotApiMethod> handle(Message message) throws Exception {

        Set<String> filters = filterHolder.getFilters(message.getChatId());

        Keyboards.InlineBuilder inlineBuilder = Keyboards.inlineBuilder();

        addCategoriesToFilter(categoryService, Constant.ORGANIZATION_BRANCH_FILTER_CALLBACK, inlineBuilder, filters);

        return Arrays.asList(
                new SendMessage()
                        .setChatId(message.getChatId())
                        .setText("Чтобы найти ближайшее к вам отделение банка, сообщите свое местоположение.")
                        .enableMarkdown(true)
                        .setReplyMarkup(Keyboards.replyBuilder()
                                .addRequestLocationButton(Constant.GIVE_LOCATION_COMMAND)
                                .addButton(Constant.BACK_TO_START_COMMAND)
                                .build()),
                new SendMessage()
                        .setChatId(message.getChatId())
                        .setText("Или воспользуйтесь фильтром:")
                        .enableMarkdown(true)
                        .setReplyMarkup(inlineBuilder.build()));
    }
}
