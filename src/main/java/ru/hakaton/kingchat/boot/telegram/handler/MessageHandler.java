package ru.hakaton.kingchat.boot.telegram.handler;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.util.List;

public interface MessageHandler {
    List<BotApiMethod> handle(Message message) throws Exception;
}
