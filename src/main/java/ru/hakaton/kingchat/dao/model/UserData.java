package ru.hakaton.kingchat.dao.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "user_data")
@Getter
@Setter
@NoArgsConstructor
public class UserData {

    @Id
    @Column(name = "chat_id")
    private Long chatId;

    @Column(name = "phone")
    private String phone;

    @Column(name = "filters")
    private String filters;
}
