package ru.hakaton.kingchat.dao.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "category")
@Getter
@Setter
@NoArgsConstructor
public class Category {

    @Id
    @SequenceGenerator(name="category_seq_gen",allocationSize=1,sequenceName = "category_seq")
    @GeneratedValue(strategy=   GenerationType.SEQUENCE, generator="category_seq_gen")
    @Column(name = "id")
    private Long id;

    @Column(name = "value")
    private String value;

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "categories")
    private List<Office> offices;
}
