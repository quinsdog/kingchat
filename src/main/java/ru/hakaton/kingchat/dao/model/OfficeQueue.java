package ru.hakaton.kingchat.dao.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@DynamicUpdate
@Table(name = "office_queue")
@Getter
@Setter
@NoArgsConstructor
public class OfficeQueue {

    @Id
    @SequenceGenerator(name="category_seq_gen",allocationSize=1,sequenceName = "category_seq")
    @GeneratedValue(strategy=   GenerationType.SEQUENCE, generator="category_seq_gen")
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "office_id", referencedColumnName = "id", nullable = false)
    private Office office;

    @Column(name = "client_phone", nullable = false)
    private String clientPhone;

    @Column(name = "client_email")
    private String clientEmail;

    @Column(name = "reg_time", nullable = false)
    private LocalDateTime regTime;

    @Column(name = "queue_time")
    private LocalDateTime queueTime;

    @Column(name = "queue_number", nullable = false)
    private String queueNumber;

    @Column(name = "active")
    private boolean isActive;
}
