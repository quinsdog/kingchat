package ru.hakaton.kingchat.dao.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "bank_appointment")
@Getter
@Setter
@NoArgsConstructor
public class BankAppointment {

    @Id
    @SequenceGenerator(name="appointment_seq_gen",allocationSize=1,sequenceName = "appointment_seq")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="appointment_seq_gen")
    @Column(name = "id")
    private Long id;

    @Column(name = "chat_id")
    private Long chatId;

    @Column(name = "bank_id")
    private String bankId;

    @Column(name = "bank_pseudo_code")
    private String bankPseudoCode;

    @Column(name = "appointment_date")
    private Date appointmentDate;

    @Column(name = "expired")
    private Boolean expired;
}
