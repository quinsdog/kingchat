package ru.hakaton.kingchat.dao.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collections;
import java.util.List;

@Entity
@Table(name = "office")
@Getter
@Setter
@NoArgsConstructor
public class Office {
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "name_raw")
    private String name;


    @Column(name = "address_raw")
    private String addres;

    @Column(name = "country_raw")
    private String country;

    @Column(name = "link_url")
    private String linkUrl;

    @Column(name = "working_time_raw")
    private String workingTimeRaw;

    @Column(name = "lat")
    private Double latitude;

    @Column(name = "lon")
    private Double longitude;

    @ManyToMany
    @JoinTable(name="office_category",
            joinColumns = @JoinColumn(name="office_id", referencedColumnName="id"),
            inverseJoinColumns = @JoinColumn(name="category_id", referencedColumnName="id")
    )
    private List<Category> categories = Collections.emptyList();

    @OneToMany(mappedBy = "office", cascade = CascadeType.ALL)
    private List<OfficeQueue> officeQueue;
}
