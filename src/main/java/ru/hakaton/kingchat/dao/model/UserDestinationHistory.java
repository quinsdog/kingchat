package ru.hakaton.kingchat.dao.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "user_destination_history")
@Getter
@Setter
@NoArgsConstructor
public class UserDestinationHistory {

    @Id
    @SequenceGenerator(name="destination_seq_gen",allocationSize=1,sequenceName = "destination_seq")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="destination_seq_gen")
    @Column(name = "id")
    private Long id;

    @Column(name = "chat_id")
    private Long chatId;

    @Column(name = "lat")
    private Double latitude;

    @Column(name = "lon")
    private Double longitude;

}
