package ru.hakaton.kingchat.dao.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.hakaton.kingchat.dao.model.BankAppointment;

import java.util.List;

@Repository
public interface BankAppointmentRepository extends CrudRepository<BankAppointment, Long> {

    @Query("from BankAppointment where chat_id = :chatId and expired = false")
    BankAppointment findCurrentBankAppointment(@Param("chatId") Long chatId);

    @Query("from BankAppointment where appointmentDate is not null " +
            "and  appointmentDate > current_timestamp and expired = false")
    List<BankAppointment> findAppointmentsForNotification();

    @Transactional
    @Modifying
    @Query("update BankAppointment set expired = true where appointmentDate is not null " +
            "and appointmentDate < current_timestamp")
    void updateAllExpired();
}
