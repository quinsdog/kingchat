package ru.hakaton.kingchat.dao.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.hakaton.kingchat.dao.model.Category;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long> {
    Category findFirstByValue(String value);
}
