package ru.hakaton.kingchat.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.hakaton.kingchat.dao.model.Office;
import ru.hakaton.kingchat.dao.model.OfficeQueue;

import java.time.LocalDateTime;
import java.util.List;

public interface QueueRepository extends JpaRepository<OfficeQueue, Long> {

    OfficeQueue findByQueueNumber(String queueNumber);

    List<OfficeQueue> findByOffice(Office office);

    /*@Query("select q from OfficeQueue q " +
            "where q.office = office and q.queueTime > startDateTime and q.queueTime < endDateTime")
    List<OfficeQueue> findAllByOfficeAndDesireTime(@Param("office") Office office,
                                                   @Param("startDateTime") LocalDateTime startDateTime,
                                                   @Param("endDateTime") LocalDateTime endDateTime);*/
}