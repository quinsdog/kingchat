package ru.hakaton.kingchat.dao.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.hakaton.kingchat.dao.model.Office;
import ru.hakaton.kingchat.dao.model.UserData;

@Repository
public interface UserDataRepository extends CrudRepository<UserData, Long> {

    @Query("from UserData where chatId = :chatId")
    UserData findByChatId(@Param("chatId") Long chatId);
}
