package ru.hakaton.kingchat.dao.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.hakaton.kingchat.dao.model.BankAppointment;
import ru.hakaton.kingchat.dao.model.UserDestinationHistory;

@Repository
public interface UserDestinationHistoryRepository extends CrudRepository<UserDestinationHistory, Long> {
}
