package ru.hakaton.kingchat.dao.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.hakaton.kingchat.dao.model.Office;

import java.util.List;

@Repository
public interface OfficeRepository extends CrudRepository<Office, String>, JpaSpecificationExecutor<Office> {

    @Query("from Office where id = :id")
    Office findOffice(@Param("id") String bankId);

    @Query("from Office where lat > :minLat and lat < :maxLat and lon > :minLon and lon < :maxLon")
    List<Office> findOfficesInRadius(@Param("minLat") float minLat, @Param("maxLat") float maxLat,
                                     @Param("minLon") float minLon, @Param("maxLon") float maxLon);
}
