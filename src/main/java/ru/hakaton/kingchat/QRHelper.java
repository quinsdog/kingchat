package ru.hakaton.kingchat;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class QRHelper {

    public static byte[] generateQRCodeImagePng(String text, int width, int height)
            throws WriterException, IOException {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);

        //Path path = Files.createTempFile("qr", text);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        MatrixToImageWriter.writeToStream(bitMatrix, "PNG", baos);
        //MatrixToImageWriter.writeToPath(bitMatrix, "PNG", path);

        return baos.toByteArray();
    }

}
