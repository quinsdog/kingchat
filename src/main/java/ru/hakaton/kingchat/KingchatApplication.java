package ru.hakaton.kingchat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class KingchatApplication {

	public static void main(String[] args) {
		SpringApplication.run(KingchatApplication.class, args);
	}

}
