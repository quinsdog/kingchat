package ru.hakaton.kingchat.parsing.dto;

import lombok.Data;

@Data
public class Phone {
    String number;
    String ext;
    String type;
    String info;
}
