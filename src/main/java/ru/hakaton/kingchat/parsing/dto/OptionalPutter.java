package ru.hakaton.kingchat.parsing.dto;

import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.util.Map;

public class OptionalPutter {
    private Map<String, String> map;

    OptionalPutter(Map<String, String> map) {
        this.map = map;
    }

    void putOptional(String key, String value) {
        Assert.isTrue(!StringUtils.isEmpty(key));

        if (!StringUtils.isEmpty(value)) {
            map.put(key, value);
        }
    }
}
