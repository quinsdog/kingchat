package ru.hakaton.kingchat.parsing.dto;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.core.io.ClassPathResource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UsibXmlParserJackson {

    public static void main(String[] args) throws Exception{
        XmlMapper xmlMapper = new XmlMapper();

       Companies companies = xmlMapper.readValue(new ClassPathResource("usib.xml").getFile(), Companies.class);

       Map<String, List<String>> multimap = new HashMap<>();

        companies.getCompany().stream()
                .map(Company::getEnumeratedFeatures)
                .flatMap(List::stream)
                .distinct()
                .forEach(keyValueItem -> {
                    List<String> strings = multimap.computeIfAbsent(keyValueItem.getName(), s -> {
                        return new ArrayList<>();
                    });
                    strings.add(keyValueItem.getValue());
                });

        System.out.println(multimap);



    }
}
