package ru.hakaton.kingchat.parsing.dto;

import lombok.Data;

@Data
public class Coordinates {
    private Double lon;
    private Double lat;
}
