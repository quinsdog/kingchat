package ru.hakaton.kingchat.parsing.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import lombok.Data;

@Data
public class WorkingTime {
    @JacksonXmlProperty(isAttribute = true)
    private String lang;

    @JacksonXmlText
    private String value;
}
