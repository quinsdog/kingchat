package ru.hakaton.kingchat.parsing.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;

import java.util.List;

@Data
@JacksonXmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class Companies {

    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Company> company;
}
