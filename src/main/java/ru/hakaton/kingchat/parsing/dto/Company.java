package ru.hakaton.kingchat.parsing.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonMerge;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.OptBoolean;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;

import java.util.List;

@Data
@JacksonXmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class Company {
    @JsonProperty("company-id")
    private String id;

    @JsonProperty("name")
    private CompanyName name;


    @JsonProperty("address")
    private Address address;

    @JsonProperty("working-time")
    private WorkingTime workingTime;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JsonProperty("phone")
    @JsonMerge(OptBoolean.TRUE)
    private List<Phone> phones;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JsonProperty("feature-enum-multiple")
    private List<KeyValueItem> enumeratedFeatures;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JsonProperty("feature-boolean")
    private List<KeyValueItem> booleanFeatures;

    @JsonProperty("coordinates")
    private Coordinates coordinates;

    @JsonProperty("info-page")
    private String url;

    @JsonProperty("country")
    private Country country;


    public void setEnumeratedFeatures(List<KeyValueItem> enumeratedFeatures) {
        if(this.enumeratedFeatures == null) {
            this.enumeratedFeatures = enumeratedFeatures;
            return;
        }

        this.enumeratedFeatures.addAll(enumeratedFeatures);
    }

    public void setBooleanFeatures(List<KeyValueItem> booleanFeatures) {
        if(this.booleanFeatures == null) {
            this.booleanFeatures = booleanFeatures;
            return;
        }

        this.booleanFeatures.addAll(booleanFeatures);
    }
}
