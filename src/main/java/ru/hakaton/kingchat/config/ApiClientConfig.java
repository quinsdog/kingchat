package ru.hakaton.kingchat.config;

import feign.Feign;
import feign.Logger;
import feign.Request;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.jaxrs.JAXRSContract;
import feign.slf4j.Slf4jLogger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.hakaton.kingchat.service.here.HereGeocodeApiClient;
import ru.hakaton.kingchat.service.here.HereRoutingApiClient;


@Configuration
public class ApiClientConfig {

    private static final int READ_TIMEOUT = 2 * 60 * 1000; // 2 min

    @Bean
    public HereRoutingApiClient hereRoutingApiClient() {

        return Feign.builder()
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
                .contract(new JAXRSContract())
                .logger(new Slf4jLogger())
                .logLevel(Logger.Level.FULL)
                .options(new Request.Options(10 * 1000, READ_TIMEOUT))
                .target(HereRoutingApiClient.class, "https://route.api.here.com");
    }

    @Bean
    public HereGeocodeApiClient hereGeocodeApiClient() {

        return Feign.builder()
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
                .contract(new JAXRSContract())
                .logger(new Slf4jLogger())
                .logLevel(Logger.Level.FULL)
                .options(new Request.Options(10 * 1000, READ_TIMEOUT))
                .target(HereGeocodeApiClient.class, "https://geocoder.api.here.com");
    }


}
