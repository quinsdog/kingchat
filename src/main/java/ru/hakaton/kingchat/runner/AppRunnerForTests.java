package ru.hakaton.kingchat.runner;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import ru.hakaton.kingchat.dao.model.Category;
import ru.hakaton.kingchat.dao.model.Office;
import ru.hakaton.kingchat.dao.repository.CategoryRepository;
import ru.hakaton.kingchat.dao.repository.OfficeRepository;
import ru.hakaton.kingchat.dao.repository.QueueRepository;
import ru.hakaton.kingchat.service.XmlParserFeaturesService;
import ru.hakaton.kingchat.service.here.HereService;
import ru.hakaton.kingchat.service.here.dto.SimpleGeoPoint;
import ru.hakaton.kingchat.service.queue.QueueService;
import ru.hakaton.kingchat.service.queue.dto.Queue;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.StreamSupport;


@Slf4j
@Component
public class AppRunnerForTests implements CommandLineRunner {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private XmlParserFeaturesService featuresService;

    @Autowired
    private HereService hereService;

    @Autowired
    private OfficeRepository officeRepository;

    @Autowired
    private QueueService queueService;

    @Override
    public void run(String... args) throws Exception {
        makeCategoryCsv();
        testHereService();
        testQueueMethods();
    }

    private void testQueueMethods() {
        /*Optional<Office> office = officeRepository.findById("2779");
        Queue queue3 = queueService.addClient(office.get(), "89182312d");
        System.out.println(queue3);
        Queue queue = queueService.addClient(office.get(), "891812323", LocalDateTime.now());
        System.out.println(queue);
        Queue queue2 = queueService.addClient(office.get(), "891823", LocalDateTime.now().plusMinutes(5));
        System.out.println(queue2);*/
    }


    private void makeCategoryCsv() throws Exception {
/*        featuresService.getCompanies().stream()
                .forEach(company -> {
                    company.getEnumeratedFeatures().forEach(keyValueItem -> {
                        String tag = keyValueItem.getName() + keyValueItem.getValue();
                        Category firstByValue = categoryRepository.findFirstByValue(tag);
                        if(firstByValue == null) {
                            Category category = new Category();
                            category.setValue(tag);
                            categoryRepository.save(category);
                        }
                    });
                });*/
    }

    private void makeOfficeCsv() throws Exception {
        //Map<String, List<String>> allFeatures = featuresService.getAllFeatures();
        /*List<LinkedHashMap<String, Object>> collect = featuresService.getCompanies().stream()
                .map(company -> {
                    LinkedHashMap<String, Object> map = new LinkedHashMap<>();

                    map.put("id", company.getId());
                    map.put("name_raw", company.getName().getName());
                    map.put("address_raw", company.getAddress().getAddress());
                    map.put("country_raw", company.getCountry().getCountryName());
                    map.put("link_url", company.getUrl());
                    map.put("working_time_raw", Optional.ofNullable(company.getWorkingTime()).map(WorkingTime::getValue).orElse(""));
                    map.put("lat", company.getCoordinates().getLat());
                    map.put("lon", company.getCoordinates().getLon());

                    return map;
                }).collect(Collectors.toList());

        System.out.println(collect.get(0).keySet());

        //System.out.println(collect);

        CsvSchema schema = CsvSchema.builder()
                .addColumn("id")
                .addColumn("name_raw")
                .addColumn("address_raw")
                .addColumn("country_raw")
                .addColumn("link_url")
                .addColumn("working_time_raw")
                .addColumn("lat")
                .addColumn("lon")
                .build();

        CsvMapper csvMapper = new CsvMapper();
        ObjectWriter writer = csvMapper.writer(schema);*/

        /*//String s = writer.writeValueAsString(collect);

        System.out.println(s);*/
    }

    private void testHereService() {
        /*Iterable<Office> all = officeRepository.findAll();
        SimpleGeoPoint simpleGeoPoint = hereService.geoCode("улица космонавта Волкова, дом 6А");
        StreamSupport.stream(all.spliterator(), false)
                .filter(office -> isValidAddress(office) && office.getLatitude() != null && office.getLongitude() != null)
                .forEach(office -> System.out.println(String.format("Office [%s], mapped coordinates [%s], FROM HERE coordinates [%s], TRAVEL TIME from us [%s], DISTANCE from us [%s]"
                , office.getId()
                , String.format("%s,%s", office.getLatitude(), office.getLongitude())
                , hereService.geoCode(office.getAddres())
                , hereService.calculateRoute(simpleGeoPoint, new SimpleGeoPoint(office.getLatitude(), office.getLongitude())).getTravelTime()
                , hereService.calculateRoute(simpleGeoPoint, new SimpleGeoPoint(office.getLatitude(), office.getLongitude())).getDistance())));*/
    }

    private boolean isValidAddress(Office office) {
        List<String> invalidAddressesId = Arrays.asList("228", "287");
        return !invalidAddressesId.contains(office.getId());
    }
}
